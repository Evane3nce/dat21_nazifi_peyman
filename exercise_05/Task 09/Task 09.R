
data <- split(mtcars, mtcars$cyl)
str(data)


models <- map(1:3, function(x) lm(data[[x]]$mpg ~ data[[x]]$wt + data[[x]]$hp))

results <- list()
for (i in c(1:3)){
  results[[i]] <- summary(models[[i]])
}

results2 <-map(models, summary)

r_squared <- list("cyl_4" = results[[1]]$r.squared,
                  "cyl_6" = results[[2]]$r.squared,
                  "cyl_4" = results[[3]]$r.squared)

r_squared


r_squared2 <- map_dbl(results2, function(x) x[["r.squared"]])
