
my_list <- 
  list(
  "a" = c(1:7),
  "name_of_second_element" = c("This","is","a",
                               "character","vector","."),
  "a_list" = list(
    c(10:4),
    c(TRUE, FALSE)
  )
)


# a) ----------------------------------------------------------------------

str(my_list)

# List of 3
# $ a                     : int [1:7] 1 2 3 4 5 6 7
# $ name_of_second_element: chr [1:6] "This" "is" "a" "character" ...
# $ a_list                :List of 2
# ..$ : int [1:7] 10 9 8 7 6 5 4
# ..$ : logi [1:2] TRUE FALSE


# b) ----------------------------------------------------------------------

my_list[["name_of_second_element"]]

# "This"      "is"        "a"         "character" "vector"    "." 


my_list[[2]]

# "This"      "is"        "a"         "character" "vector"    "." 


# c) ----------------------------------------------------------------------

my_list$a[1] # 1
my_list[["a"]][1] # 1
my_list[[1]][1] # 1


# d) ----------------------------------------------------------------------

class(my_list[2]) # list


# e) ----------------------------------------------------------------------

str(my_list$"a_list"[2])

my_list$a_list[[2]][2]

# List of 1
# $ : logi [1:2] TRUE FALSE

new_unlist <- unlist(my_list)

which(new_unlist == FALSE) # 22


# f) ----------------------------------------------------------------------

str(mtcars)
model <- lm(formula = mpg ~ wt, data = mtcars)

typeof(model) # list

str(model)

coeffs <- model$coefficients

# (Intercept)          wt 
# 37.285126   -5.344472 

str(coeffs)

#  Named num [1:2] 37.29 -5.34
# - attr(*, "names")= chr [1:2] "(Intercept)" "wt"