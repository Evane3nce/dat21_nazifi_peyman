my_vector1 = [ 1 , 2 , 3 , 4 , 1 , 2 , -5]
my_vector2 = [-1 , 4 , 0 , -5, -3, -2, -5]

result = []

product = lambda x,y: x*y

for num in map(product, my_vector1, my_vector2):
    result.append(num)

print("my_vector1 * my_vector2 = ", result)


result_non_negative = []

non_negative = lambda x: x >= 0

for num in filter(non_negative, result):
    result_non_negative.append(num)

print("non_negative(my_vector1 * my_vector2) = ", result_non_negative)


