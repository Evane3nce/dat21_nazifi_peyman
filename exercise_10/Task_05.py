result_loop = []

for num in range(6):

    if num > 3:
        result_loop.append(num**2)
        continue

    if num == 1:
        result_loop.append(-1)
        continue

    else:
        result_loop.append(num)

print("result_loop = ", result_loop)


result_list_comprehension = [num**2 if num > 3 else -1 if num == 1 else num for num in range(6)]

print("result_list_comprehension = ", result_list_comprehension)




