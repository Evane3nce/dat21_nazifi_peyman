import cmath as cm

def quadratic_solver(a, b, c):   
    delta = b**2 - 4*a*c
    X1 = (-b+cm.sqrt(delta))/(2*a)
    X2 = (-b-cm.sqrt(delta))/(2*a)

    return X1, X2

print(quadratic_solver(a = 1, b = 5, c = 6))
print(quadratic_solver(a = 2, b = 12, c = 20))
print(quadratic_solver(a = 2, b = 4, c = 2))
