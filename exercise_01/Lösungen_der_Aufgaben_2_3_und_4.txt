	

Aufgabe 2


Person A als Nazifi Peyman, und Person B als Hyeri Rhim


Erstellung der 2 python Dateien:

	$ touch analysis_a.py analysis_b.py

Das schicken der 2 Dateien zum Stage-Ort
	$ git add .

Kommiten der 2 python Dateien zum HEAD:
	$ git commit -m 'commit 2 analysis python files'
	[main c5a4372] commit 2 analysis python files
 	2 files changed, 0 insertions(+), 0 deletions(-)
 	create mode 100644 exercise_01/analysis_a.py
 	create mode 100644 exercise_01/analysis_b.py

man sieht aber das Kommiten zwei Mals gemacht wurde, zuerst analysis_a und dan analysis_b. Um es zu lösen, man muss die Dateien reseten und dann jedes einzeln kommiten.

	$ git reset --soft HEAD^

	$ git reset analysis_b.py

	$ git status
	On branch main
	Your branch is ahead of 'origin/main' by 2 commits.
  	(use "git push" to publish your local commits)

	Changes to be committed:
  	(use "git restore --staged <file>..." to unstage)
        	new file:   analysis_a.py

	Untracked files:
 	 (use "git add <file>..." to include in what will be committed)
       	 analysis_b.py


dann analysis_a kommiten
	$ git commit -m 'commiting analysis_a python file'
	[main 75a9df7] commiting analysis_a python file
	 1 file changed, 0 insertions(+), 0 deletions(-)
	 create mode 100644 exercise_01/analysis_a.py

Next, analysis_b addieren und dann kommiten

	$ git add analysis_b.py
	$ git commit -m 'commiting analysis_b python file'
	[main 0d4f0a1] commiting analysis_b python file
	 1 file changed, 0 insertions(+), 0 deletions(-)
	 create mode 100644 exercise_01/analysis_b.py

jetzt klonen das Githab-Konto

	$ git clone https://gitlab.com/Evane3nce/dat21_nazifi_peyman.git
	Cloning into 'dat21_nazifi_peyman'...
	remote: Enumerating objects: 3, done.
	remote: Counting objects: 100% (3/3), done.
	remote: Compressing objects: 100% (2/2), done.
	remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
	Receiving objects: 100% (3/3), done.

am Ende, alle neue Dateien und Änderungen zum Remote zu pushen
	$ git push
	Enumerating objects: 10, done.
	Counting objects: 100% (10/10), done.
	Delta compression using up to 8 threads
	Compressing objects: 100% (7/7), done.
	Writing objects: 100% (9/9), 958 bytes | 958.00 KiB/s, done.
	Total 9 (delta 1), reused 0 (delta 0), pack-reused 0
	To https://gitlab.com/Evane3nce/dat21_nazifi_peyman.git
   	9fe0560..0d4f0a1  main -> main


###################################################################

Aufgabe 3


Person A als Nazifi Peyman, und Person B als Hyeri Rhim

Person A: Erstellung neue Dateien

	$ touch main.log
	$ mkdir testdata
	$ mkdir playground
	$ cd playground/
	$ touch hello.py error.log

Perswon A: Erstellung das .gitignore Datei

	Peyman@DESKTOP-HJULCGE MINGW64 /d/FHJ/2021/SDS/dat21_nazifi_peyman/exercise_01/playground (main)	
	$ cd ..

	$ touch .gitignore


Konfigurieren .gitignore, um die notwendige Bediengungen zu erfüllen

	$ nano .gitignore

	playground/*.log
	testdata/*
	!testdata/.gitkeep

Es ist notwendig, eine Datei .gitkeep in der Ordner testdata/ zu erstellen.
	Peyman@DESKTOP-HJULCGE MINGW64 /d/FHJ/2021/SDS/dat21_nazifi_peyman/exercise_01 (main)
	$ cd testdata/

	$ touch .gitkeep

	Peyman@DESKTOP-HJULCGE MINGW64 /d/FHJ/2021/SDS/dat21_nazifi_peyman/exercise_01/testdata (main)
	$ cd ..

	Peyman@DESKTOP-HJULCGE MINGW64 /d/FHJ/2021/SDS/dat21_nazifi_peyman/exercise_01 (main)
	$ git status
	On branch main
	Your branch is up to date with 'origin/main'.

	Untracked files:
	  (use "git add <file>..." to include in what will be committed)
       	 	.gitignore
        	dat21_nazifi_peyman/
        	main.log
        	playground/
        	testdata/

	nothing added to commit but untracked files present (use "git add" to track)


Person A: addieren, kommiten und pushen alle Änderungen

	$ git add .
	warning: LF will be replaced by CRLF in exercise_01/.gitignore.
	The file will have its original line endings in your working directory
	warning: adding embedded git repository: exercise_01/dat21_nazifi_peyman
	hint: You've added another git repository inside your current repository.
	hint: Clones of the outer repository will not contain the contents of
	hint: the embedded repository and will not know how to obtain it.
	hint: If you meant to add a submodule, use:
	hint:
	hint:   git submodule add <url> exercise_01/dat21_nazifi_peyman
	hint:
	hint: If you added this path by mistake, you can remove it from the
	hint: index with:
	hint:
	hint:   git rm --cached exercise_01/dat21_nazifi_peyman
	hint:
	hint: See "git help submodule" for more information.


	$ git commit -m 'commiting new files'
	[main 478122b] commiting new files
	 5 files changed, 5 insertions(+)
 	create mode 100644 exercise_01/.gitignore
 	create mode 160000 exercise_01/dat21_nazifi_peyman
 	create mode 100644 exercise_01/main.log
	 create mode 100644 exercise_01/playground/hello.py
 	create mode 100644 exercise_01/testdata/.gitkeep


	$ git push
	Enumerating objects: 8, done.
	Counting objects: 100% (8/8), done.
	Delta compression using up to 8 threads
	Compressing objects: 100% (3/3), done.
	Writing objects: 100% (6/6), 643 bytes | 643.00 KiB/s, done.
	Total 6 (delta 0), reused 0 (delta 0), pack-reused 0
	To https://gitlab.com/Evane3nce/dat21_nazifi_peyman.git
  	0d4f0a1..478122b  main -> main

Person B sieht sich den log an.
Zuerst klonen das Projekt vom remote Repo wobei Person B ein Maiantainer ist. 
 
	$ git clone https://gitlab.com/Evane3nce/dat21_nazifi_peyman.git
	Cloning into 'dat21_nazifi_peyman'...
	remote: Enumerating objects: 18, done.
	remote: Counting objects: 100% (18/18), done.
	remote: Compressing objects: 100% (12/12), done.
	remote: Total 18 (delta 1), reused 0 (delta 0), pack-reused 0
	Receiving objects: 100% (18/18), 4.33 KiB | 2.17 MiB/s, done.
	Resolving deltas: 100% (1/1), done.

	$ git log
	commit 478122ba87be5b70c40b1fa1bf1b8800b46f9768 (HEAD -> main, oriin/HEAD)
	Author: Peyman <peyman.nazifi@edu.fh-joanneum.at>
	Date:   Wed Oct 20 10:19:34 2021 +0200

    	commiting new files

	commit 0d4f0a18dc4f4ea74f2a6463033bca91257103a7
	Author: Peyman <peyman.nazifi@edu.fh-joanneum.at>
	Date:   Wed Oct 20 09:55:41 2021 +0200

    	commiting analysis_b python file

	commit 75a9df7fdf9adc165f32455f75a2529144d6d745
	Author: Peyman <peyman.nazifi@edu.fh-joanneum.at>
	Date:   Wed Oct 20 09:54:41 2021 +0200

   	commiting analysis_a python file

	commit d1ec803a90944ce1962d7f89c41992a1135c927d
	Author: Peyman <peyman.nazifi@edu.fh-joanneum.at>
	Date:   Wed Oct 20 09:51:41 2021 +0200

    	deleting previous files


#############################################################
Aufgabe 4


Person A als Peyman Nazifi, und Person B als Hyeri Rhim

Person B erstellt einen neuen Branch.

	$ git branch python-feature
	$ git checkout python-feature


Person B erstellt zwei Dateien und zwei Ordner.
	$ mkdir src
	$ cd src/
	$ touch main.py readme.md
	$ mkdir unittest

Weil eine leere Ordner (unittest/) nicht addiert werden kann, wird eine Testdatei drin erstellt.
	$ touch unittest_content.txt$ 
	$ git add unittest/
	$ git commit -m "adds bcz it was missed for last commit"
	[python-feature aeeb25a] adds bcz it was missed for last commit
 	1 file changed, 0 insertions(+), 0 deletions(-)
 	create mode 100644 exercise_01/src/unittest/unittest_content.txt

Person A editiert die Datei hello.py.

	Peyman@DESKTOP-HJULCGE MINGW64 /d/FHJ/2021/SDS/dat21_nazifi_peyman/exercise_01 (main)
	$ cd playground/

	$ nano hello.py

Person A: addieren und kommiten und pushen die geänderte hello.py.
	$ git add .
	warning: LF will be replaced by CRLF in exercise_01/playground/hello.py.
	The file will have its original line endings in your working directory

	$ git commit -m 'commiting hello world python chanded file'
	[main f5f93cc] commiting hello world python chanded file
 	1 file changed, 1 insertion(+)

	$ git push
	Enumerating objects: 9, done.
	Counting objects: 100% (9/9), done.
	Delta compression using up to 8 threads
	Compressing objects: 100% (3/3), done.
	Writing objects: 100% (5/5), 437 bytes | 437.00 KiB/s, done.
	Total 5 (delta 1), reused 0 (delta 0), pack-reused 0
	To https://gitlab.com/Evane3nce/dat21_nazifi_peyman.git
   	478122b..f5f93cc  main -> main

Person B pusht den Branch python-feature.
	$ git push --set-upstream origin python-feature
	Enumerating objects: 18, done.
	Counting objects: 100% (18/18), done.
	Delta compression using up to 8 threads
	Compressing objects: 100% (11/11), done.
	Writing objects: 100% (18/18), 4.33 KiB | 4.33 MiB/s, done.
	Total 18 (delta 1), reused 18 (delta 1), pack-reused 0
	remote:
	remote: To create a merge request for python-feature, visit:
	remote:   https://gitlab.com/Evane3nce/dat21_nazifi_peyman/-/merge_requests/new?merge_request%5Bsource_branc%5D=python-feature
	remote:
	To https://gitlab.com/Evane3nce/dat21_nazifi_peyman.git
	 * [new branch]      python-feature -> python-feature
	Branch 'python-feature' set up to track remote branch 'python-feature' from 'origin'.

Person A: pullen alle neue Änderungen, die Person B in master und feature gemacht hat
	Peyman@DESKTOP-HJULCGE MINGW64 /d/FHJ/2021/SDS/dat21_nazifi_peyman/exercise_01/playground (main)
	$ cd ..

	$ git checkout python-feature
	Switched to a new branch 'python-feature'
	Branch 'python-feature' set up to track remote branch 'python-feature' from 'origin'.

	Peyman@DESKTOP-HJULCGE MINGW64 /d/FHJ/2021/SDS/dat21_nazifi_peyman/exercise_01 (python-feature)
	$ git pull
	Already up to date.

Person B merges den Branch python-feature intden main branch.
Zuerst pullen das remote Repo. 
	$ git checkout main
	Switched to branch 'main'
	Your branch is up to date with 'origin/main'.

	Hyeri@LAPTOP-TX-WIN-FBD90JN MINGW64 ~/Desktop/myGitProjects/dat21_man/exercise_01 (main)
	$ git pull
	remote: Enumerating objects: 9, done.
	remote: Counting objects: 100% (9/9), done.
	remote: Compressing objects: 100% (3/3), done.
	remote: Total 5 (delta 1), reused 0 (delta 0), pack-reused 0
	Unpacking objects: 100% (5/5), 417 bytes | 10.00 KiB/s, done.
	From https://gitlab.com/Evane3nce/dat21_nazifi_peyman
   	478122b..f5f93cc  main       -> origin/main
	Updating 478122b..f5f93cc
	Fast-forward
 	exercise_01/playground/hello.py | 1 +
 	1 file changed, 1 insertion(+)

Dann wird den Branch merged.
	$ git merge python-feature
	Merge made by the 'recursive' strategy.
 	exercise_01/src/main.py                       | 0
 	exercise_01/src/readme.md                     | 0
 	exercise_01/src/unittest/unittest_content.txt | 0
	3 files changed, 0 insertions(+), 0 deletions(-)
 	create mode 100644 exercise_01/src/main.py
 	create mode 100644 exercise_01/src/readme.md
	create mode 100644 exercise_01/src/unittest/unittest_content.txt

Person B sieht ob der Branch wurde schon merged.
	$ git branch --merged main
	* main
  	python-feature

Person A: Nachschauen das Log Datei, welche enthält alle bearbeitungen
	Peyman@DESKTOP-HJULCGE MINGW64 /d/FHJ/2021/SDS/dat21_nazifi_peyman/exercise_01 (python-feature)
	$ git log
	commit aeeb25a4aff09e06bebcdab7d9687713f07239a1 (HEAD -> python-feature, origin/python-feature)
	Author: hyeri_rhim <hyeri.rhim@edu.fh-joanneum.at>
	Date:   Wed Oct 20 10:46:23 2021 +0200

    	adds bcz it was missed for last commit

	commit 840d664af01d7afbfd800b119ce3e6bafd961800
	Author: hyeri_rhim <hyeri.rhim@edu.fh-joanneum.at>
	Date:   Wed Oct 20 10:38:41 2021 +0200

    	creates empty files

	commit 478122ba87be5b70c40b1fa1bf1b8800b46f9768
	Author: Peyman <peyman.nazifi@edu.fh-joanneum.at>
	Date:   Wed Oct 20 10:19:34 2021 +0200

    	commiting new files

	commit 0d4f0a18dc4f4ea74f2a6463033bca91257103a7
	Author: Peyman <peyman.nazifi@edu.fh-joanneum.at>
	Date:   Wed Oct 20 09:55:41 2021 +0200

    	commiting analysis_b python file

	commit 75a9df7fdf9adc165f32455f75a2529144d6d745
	Author: Peyman <peyman.nazifi@edu.fh-joanneum.at>
	Date:   Wed Oct 20 09:54:41 2021 +0200

    	commiting analysis_a python file

	commit d1ec803a90944ce1962d7f89c41992a1135c927d
	Author: Peyman <peyman.nazifi@edu.fh-joanneum.at>
	Date:   Wed Oct 20 09:51:41 2021 +0200

    	deleting previous files

	commit 8675e35c960adc8f03eeda295be71f2661c444a3

(End of Text)
