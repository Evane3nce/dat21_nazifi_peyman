

sizes <- sample(150:196, size = 100, replace = TRUE)

fct_1 <- fct_recode(cut(sizes, breaks = 3), "small" = "(150,165]", "average" = "(165,181]", 
                                                    "large" = "(181,196]")



fct_2 <- cut(sizes, breaks = 3, labels = c("small", "average", "large"))


