my_dict = {}

print(type(my_dict))

my_dict["name"] = "Peyman Nazifi"

print(my_dict)

my_dict[0] = [1, 2, 3, 4, 5]

print(my_dict)

my_dict["age"] = 99

print(my_dict)

my_dict.update({"age":25})

print(my_dict)

my_dict.pop("age")

print(my_dict)


print([*my_dict])
# gives a list of keys

