f_a = 1.1
f_b = 1.7

i_a = int(f_a)
i_b = int(f_b)

print("f_a = ", f_a)
print("f_b = ", f_b)

print("i_a = ", i_a)
print("i_b = ", i_b)


ri_a = round(f_a)
ri_b = round(f_b)

print("ri_a = ", ri_a)
print("ri_b = ", ri_b)

f_a += 0.1
f_b += 0.2

print("f_a = ", f_a)
print("f_b = ", f_b)
# yes, because variable are stored binary and converted to dec when printed

# (1.1)_decimal = (1.00110011001100110011)_binary
# (1.00110011001100110011)_binary = (1.19999980926513671875)_deccimal


