# Tuples
# Tuples are in many ways similar to lists, but they are defined with parentheses rather than square brackets:
t = (1, 2, 3)

# They can also be defined without any brackets at all:
t = 1, 2, 3
print(t)

# tuples have a length, and individual elements can be extracted using square-bracket indexing:
print(len(t))
print(t[0])

# The main distinguishing feature of tuples is that they are immutable: 
# this means that once they are created, their size and contents cannot be changed:

#t[1] = 4
# t.append(4)

print("range(10) is equal to ", range(10))
tpl = range(10)
t = tuple(tpl)
print(t)

# t[0] = 10 # Error, tuples are immutable

# tuples can be used as indexes of dictionaries (as well as values)
my_dict = {(1,1): 1, (2,2): 2, (3,3): 3, (4,4, 4): (4, 4)}
print(my_dict)
