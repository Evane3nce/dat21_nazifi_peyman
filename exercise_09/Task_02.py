import cmath as cm

c_num = 1 + 2j

print(type(c_num))

r_num = 1.01

print(type(r_num))

print(r_num + c_num)

print(type(r_num + c_num))

print(cm.phase(c_num))

angle_degree = cm.phase(c_num) * 180/cm.pi

print("angle of c_num in degree", angle_degree)

print(c_num)

print(cm.sqrt(c_num))