a = 24
print("a before change is equal to ", a)

# We might want to update the variable a with this new value; 
# in this case, we could combine the addition and the assignment 
# and write a = a + 2. Because this type of combined operation and assignment is so common, 
# Python includes built-in update operators for all of the arithmetic operations:

a += 2  # equivalent to a = a + 2
print("a after change is equal to ", a)

# There is an augmented assignment operator corresponding to each of the binary operators listed earlier; 
# in brief, they are:

# a += b| a -= b|a *= b| a /= b| |a //= b| a %= b|a **= b|a &= b| |a |= b| a ^= b|a <<= b| a >>= b

# Each one is equivalent to the corresponding operation followed by assignment: that is, 
# for any operator "■", the expression a ■= b is equivalent to a = a ■ b, with a slight catch. 
# For mutable objects like lists, arrays, or DataFrames, these augmented assignment operations 
# are actually subtly different than their more verbose counterparts: 
# they modify the contents of the original object rather than creating a new object to store the result.

x = 5
print("x before change is equal to ", x)

x *= 2
print("x after change is equal to ", x)


# Identity and Membership Operators

# Like and, or, and not, Python also contains prose-like operators to check for identity and membership. 
# They are the following:

# Operator	Description
# a is b	True if a and b are identical objects (Identity)
# a is not b	True if a and b are not identical objects (Identity)
# a in b	True if a is a member of b (Membership)
# a not in b	True if a is not a member of b (Membership)

a = [1, 2, 3]
b = [1, 2, 3]

print("a == b is equal to ", a == b)
print("a is b is equal to ", a is b)
print("a is not b is equal to ", a is not b)

# What do identical objects look like? Here is an example:

X = [1, 2, 3]
Y = X
print("X is Y is equal to ", X is Y)

# The difference between the two cases here is that in the first, 
# a and b point to different objects, while in the second they point to the same object. 
# As we saw in the previous section, Python variables are pointers. 
# The "is" operator checks whether the two variables are pointing to the same container (object), 
# rather than referring to what the container contains. 

# Membership operators

# Membership operators check for membership within compound objects. So, for example, we can write:

print("1 in [1, 2, 3] is equal to ", 1 in [1, 2, 3])

print("2 not in [1, 2, 3] is equal to ", 2 not in [1, 2, 3])


# Don't compare boolean values to True or False using ==:

# Correct:
# if greeting:

# Wrong:
# if greeting == True:

# Worse:
# if greeting is True:






