my_list = ["one", "two", "three", "four", "five", 
            "six", [1,2,3]]

print(my_list[1])

print(my_list[2:])

my_list[-1][-1] = 0

print(my_list[-1])

my_list[-1].append(2)

my_list[-1].append(1)

print(my_list[-1])

my_list2 = my_list[-1][::2]

print(my_list2)