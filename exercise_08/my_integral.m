
function y = my_integral(alpha, k)

  y = (alpha * k^2) / 2 

end
