
function y = my_harmonic_mean(x)
  sum_inverse = 0; 

  for i= 1:length(x)
    sum_inverse = sum_inverse + (1/x(i));
  end

  y = length(x)/sum_inverse; 

end
