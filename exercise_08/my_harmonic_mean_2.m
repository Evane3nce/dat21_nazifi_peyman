
# If the special parameter name varargin appears at the end of 
# a function parameter list it indicates that the function takes 
# a variable number of input arguments.

# nargin Report the number of input arguments to a function.


function y = my_harmonic_mean_2(varargin)
  mean = [];
  
  for i =1:nargin
    mean = [mean, my_harmonic_mean(varargin{i})];
  end

  y = mean;
end
