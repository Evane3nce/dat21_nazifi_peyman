% Exercise 08

%% task 01

#a

# Rep function in matlab
a=[1 2 ; 3 4];
repmat(a,2,3)

# Seq function in matlab
1:2:8 

# Which function in matlab
find(1:10 > 5.5) 

# tibble or dataframe ?

#############

#c

# diag(1,3)

eye(3)			  

# rnorm(4)
randn(1,4) 

# dim()
a=[1 2 ; 3 4];
size(a)

# Real part of a complex number Re
z = 3+4i 
real(z) 

# Imaginary part of a complex number Im 
imag(z)


%% Task 02

my_factorial(3)


%% Task 03

vector =[10, 5, 3, 4];
vector2 = [8, 7 , 5, 8];


my_harmonic_mean(vector)
my_harmonic_mean_2(vector, vector2)


pkg load statistics
harmmean(vector)
harmmean(vector2)


%% Task 04

alpha= 3;
k= 4;

my_integral(alpha, k)

integral(@(x) alpha * x, 0, k)


%% Task 05 

# A square matrix A is singular if it does not have linearly independent columns. 
# If A is singular, the solution to Ax = b either does not exist, or is not unique. 
# The backslash operator, A\b, issues a warning if A is nearly singular 
# or if it detects exact singularity.

# If A is singular and Ax = b has a solution, 
# you can find a particular solution that is not unique, by typing

# P = pinv(A)*b

# pinv(A) is a pseudoinverse of A.

A=[1 -2 3; 0 4 1; 2 2 2]; 
b=[9; -2; 4];

pinv(A)*b 

A\b




