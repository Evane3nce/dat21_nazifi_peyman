
function y = my_factorial(x)
factorial = 1; 

for i = 1:x 
  factorial = factorial * i;
end

y = factorial;
end
